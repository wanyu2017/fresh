# springboot生鲜水果商城系统蔬菜销售系统

[源码下载](http://http://7cjava.com/?p=14868)

#### 环境需要
1.运行环境：最好是java jdk 1.8，我们在这个平台上运行的。其他版本理论上也可以。
2.IDE环境：IDEA，Eclipse,Myeclipse都可以。推荐IDEA;
3.tomcat环境：Tomcat 7.x,8.x,9.x版本均可
4.硬件环境：windows 7/8/10 1G内存以上；或者 Mac OS；
5.是否Maven项目: 是；查看源码目录中是否包含pom.xml；若包含，则为maven项目，否则为非maven项目 
6.数据库：MySql 5.7版本；

#### 技术栈
1. 后端：SpringBoot
2. 前端：HTML+Thymeleaf+jQuery+easyUI+bootstrap

#### 使用说明
1. 使用Navicat或者其它工具，在mysql中创建对应名称的数据库，并导入项目的sql文件；
2. 使用IDEA/Eclipse/MyEclipse导入项目，Eclipse/MyEclipse导入时，若为maven项目请选择maven;若为maven项目，导入成功后请执行maven clean;maven install命令，配置tomcat，然后运行；
3. 将项目中application.yml配置文件中的数据库配置改为自己的配置;
4.运行项目，前台地址：http://localhost:8080
用户名 wang11 密码123456

后台地址： http://localhost:8080/toAdminLogin
用户名 supperman 密码  123456
![输入图片说明](code/%E5%89%8D%E5%8F%B0-%E9%A6%96%E9%A1%B5.png)
![输入图片说明](code/%E5%89%8D%E5%8F%B0-%E6%B3%A8%E5%86%8C.png)
![输入图片说明](code/%E5%89%8D%E5%8F%B0-%E6%B3%A8%E5%86%8C1.png)
![输入图片说明](code/%E5%90%8E%E7%AE%A1-%E8%AE%A2%E5%8D%95%E7%AE%A1%E7%90%86.png)
![输入图片说明](code/%E5%90%8E%E7%AE%A1-%E5%95%86%E5%93%81%E7%AE%A1%E7%90%86.png)
![输入图片说明](code/%E5%89%8D%E5%8F%B0-%E7%99%BB%E5%BD%95.png)
![输入图片说明](code/%E5%89%8D%E5%8F%B0-%E5%95%86%E5%93%81%E8%AF%A6%E6%83%85.png)
![输入图片说明](code/%E5%89%8D%E5%8F%B0-%E8%B4%AD%E7%89%A9%E8%BD%A6.png)


